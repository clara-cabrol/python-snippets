#!/usr/bin/python


def syracuse(k):
    if k % 2 == 0:
        return k // 2
    else:
        return 3 * k + 1


def n_termes_suite(prem, n):
    print(prem, end=" ")
    for i in range(1, n + 1):  # On calcule les termes de la suite du rang 1 au rang n
        suiv = syracuse(prem)
        print(suiv, end=" ")
        prem = suiv
    print()


terme_u0 = int(input("Saisir le premier terme de la suite :"))

rang_final = int(input("Saisir le rang du dernier terme :"))

print("La liste des termes de la suite est :")

n_termes_suite(terme_u0, rang_final)
