#!/usr/bin/python


def ConvDecEnBin(nb):
    tab = []
    while nb != 0:
        quotient = nb // 2
        reste = nb % 2
        nb = quotient
        tab.insert(0, reste)
    return str(tab)


def ConvDecEnBinRec(nb):
    resultat = ""
    quotient = nb // 2
    reste = nb % 2
    if quotient != 0:
        resultat = ConvDecEnBinRec(quotient) + str(reste)
    else:
        resultat = str(reste)
    return resultat


def ConvDecEnBinSol(nb):
    result = ""
    while nb > 0:
        nb, reste = divmod(nb, 2)
        result = str(reste) + result
    return result


def ConvDecEnBinRecSol(nb):
    nb, reste = divmod(nb, 2)
    result = str(reste)
    if nb > 0:
        result = str(ConvDecEnBinRecSol(nb)) + result
    return result


if __name__ == "__main__":
    nb_arg = int(input("nb: "))

    print(f"Conversion décimal en binaire: {ConvDecEnBin(nb=nb_arg)}")
    print(f"Conversion décimal en binaire (récursif): {ConvDecEnBinRec(nb=nb_arg)}")

    print(f"\nRéponses: {ConvDecEnBinSol(nb=nb_arg)}, {ConvDecEnBinRecSol(nb=nb_arg)}")
