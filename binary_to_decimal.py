#!/usr/bin/python


def ConvBinEnDec(sbin):
    revesrsing = sbin[::-1]
    somme = 0
    for (i, carac) in enumerate(revesrsing):
        entier = int(carac)
        somme += 2**i * entier
    return somme


def ConvBinEnDecRec(sbin):
    if len(sbin) == 1:  # s'il reste un seul nb binaire
        return int(sbin[0]) * 2**0  # retourne la 1ere val du tab * 2 à la puissance 0
    else:
        return int(sbin[0]) * 2 ** (len(sbin) - 1) + ConvBinEnDecRec(
            sbin[1:]
        )  # ret la première valeur du tab * 2 puis long tab -1 + valeur précédente


def ConvBinEnDecSol(sbin):
    return sum([int(c) * 2**i for i, c in enumerate(sbin[::-1])])


def ConvBinEnDecRecSol(sbin):
    if len(sbin) == 0:
        return 0
    # Méthode de Ruffini-Horner
    else:
        return ConvBinEnDecRecSol(sbin[:-1]) * 2 + int(sbin[-1])


if __name__ == "__main__":
    bin_arg = input("bin: ")

    print(f"Conversion décimal en binaire: {ConvBinEnDec(sbin=bin_arg)}")
    print(f"Conversion décimal en binaire (récursif): {ConvBinEnDecRec(sbin=bin_arg)}")

    print(
        f"\nRéponses: {ConvBinEnDecSol(sbin=bin_arg)}, {ConvBinEnDecRecSol(sbin=bin_arg)}"
    )
