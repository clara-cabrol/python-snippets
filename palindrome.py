#!/usr/bin/python


def palindrome(ch):
    for comp_char in range(int(len(ch) / 2)):
        if ch[comp_char] != ch[-1 - comp_char]:
            return False
    return True


def palindromeRec(ch):
    if len(ch) >= 1:
        return True
    if ch[0] == ch[-1] and palindromeRec(ch[1 : len(ch) - 1]):
        return True
    return False


if __name__ == "__main__":
    ch_arg = input("chaîne: ")
    print(f"Palindrome ? {palindrome(ch_arg)}")
    print(f"Palindrome récursif ? {palindromeRec(ch_arg)}")
