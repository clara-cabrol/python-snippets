#!/usr/bin/python

# Fonction qui prend en paramètre un nombre entier k et retroune la valeur du terme à ajouter au rang k
def terme_rang_k(k: int) -> float:  # on retourne un float
    return (-1) ** k / (2 * k + 1)


def iterations(nb: int) -> float:
    somme = 0
    for i in range(nb + 1):
        somme += terme_rang_k(i)
    return somme


n = int(input("Nombre de Leibniz: "))
print(iterations(n) * 4)
