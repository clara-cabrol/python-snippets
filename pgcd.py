#!/usr/bin/python


def pgcd(a, b):
    while a != b:
        if b < a:
            a = a - b
        else:
            b = b - a
    return a


def pgcdRec(a, b):
    if b == a:
        return a
    if b < a:
        a = a - b
    else:
        b = b - a
    return pgcdRec(a, b)


def pgcdSol(a, b):
    while a != b:
        if a < b:
            b = b - a
        else:
            a = a - b
    return a


if __name__ == "__main__":
    a = int(input("a: "))
    b = int(input("b: "))

    print(pgcd(a, b))
    print(pgcdSol(a, b))
